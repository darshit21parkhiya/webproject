import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-line-chart',
  templateUrl: './my-line-chart.component.html',
  styleUrls: ['./my-line-chart.component.less']
})
export class MyLineChartComponent implements OnInit {
  constructor() {}

  public lineChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          }
        }
      ]
    },
    // Container for pan options
    pan: {
      // Boolean to enable panning
      enabled: true,

      // Panning directions. Remove the appropriate direction to disable
      // Eg. 'y' would only allow panning in the y direction
      mode: 'xy',
      // Function called once panning is completed
      // Useful for dynamic data loading
      onPan({chart}) { console.log(`I was panned!!!`); }
    },

    // Container for zoom options
    zoom: {
      // Boolean to enable zooming
      enabled: true,

      // Zooming directions. Remove the appropriate direction to disable
      // Eg. 'y' would only allow zooming in the y direction
      mode: 'xy',

      // Function called once zooming is completed
      // Useful for dynamic data loading
      onZoom({chart}) { console.log(`I was zoomed!!!`); }
    }
  };
  public lineChartLabels = [
    'Red',
    'Blue',
    'Yellow',
    'Green',
    'Purple',
    'Orange'
  ];
  public lineChartType = 'line';
  public lineChartLegend = true;
  public lineChartData = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' }
  ];

  ngOnInit() {}
}
