import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyMultiyaxisChartComponent } from './my-multiyaxis-chart.component';

describe('MyMultiyaxisChartComponent', () => {
  let component: MyMultiyaxisChartComponent;
  let fixture: ComponentFixture<MyMultiyaxisChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyMultiyaxisChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyMultiyaxisChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
