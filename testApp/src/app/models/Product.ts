export class Product {
  public constructor(
    public pid: number,
    public pname: string,
    public pprice: number,
    public pimg: string,
    public soh: number
  ) {}
}
